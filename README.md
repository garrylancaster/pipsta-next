# pipsta-next

Scripts for using the Pipsta printer with the ZX Spectrum Next.

Download these scripts into the home directory on your Pipsta's Raspberry Pi. Currently there are two files:

| File | Use |
| ------ | ------ |
| pipsta-server.py | Listens on port 65432 and outputs data received directly to the printer |
| .profile | Automatically starts pipsta-server.py on boot/login | 